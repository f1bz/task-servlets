# Simple CRUD web app with using servlets/jsp

### Done for now (12.08.2019)
- navigation (3-4 pages)
- form - insert new element + update element 
- list page + search text field 
- data: in-memory - lists
- usage html + css 
- details page 
- input form validation 
- servlet validation 
- validator tests
- deploy to tomcat with maven   
- selenium tests 
- **_more search filters_**
- **_pagination_**