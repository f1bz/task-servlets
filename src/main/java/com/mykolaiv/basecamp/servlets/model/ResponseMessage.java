package com.mykolaiv.basecamp.servlets.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ResponseMessage {

    private final boolean isOk;
    private final Long id;
    private final Long creationTime;
    private final String rawData;

    public ResponseMessage(boolean isOk, String rawData) {
        this(isOk, null, null, rawData);
    }
}