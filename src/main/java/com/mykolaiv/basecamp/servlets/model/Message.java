package com.mykolaiv.basecamp.servlets.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

@Builder
@Data
public class Message {
    private LocalDateTime creationTime;
    private Long id;
    private String text;
    private String author;
    private Set<String> tags;
}
