package com.mykolaiv.basecamp.servlets.web.servlet.messages;

import com.mykolaiv.basecamp.servlets.model.Message;
import com.mykolaiv.basecamp.servlets.service.MessageService;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.mykolaiv.basecamp.servlets.repositiry.InMemoryMessageRepository.MAX_MESSAGES_PER_QUERY;

@Slf4j
@WebServlet("/all")
public class AllMessageServlet extends HttpServlet {

    private final MessageService messageService = MessageService.getInstance();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Message> messages = messageService.get(null, null, 0);
        request.setAttribute("messages", messages);
        request.setAttribute("messagesPerPage", MAX_MESSAGES_PER_QUERY);
        request.getRequestDispatcher("WEB-INF/messages/all_messages.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchField = request.getParameter("searchField");
        String searchValue = request.getParameter("searchValue");
        List<Message> messages = messageService.get(searchField, searchValue, 0);
        request.setAttribute("messages", messages);
        request.setAttribute("searchField", searchField);
        request.setAttribute("messagesPerPage", MAX_MESSAGES_PER_QUERY);
        request.setAttribute("searchValue", searchValue);
        request.setAttribute("startIndex", 0);
        request.getRequestDispatcher("WEB-INF/messages/all_messages.jsp").forward(request, response);
    }
}
