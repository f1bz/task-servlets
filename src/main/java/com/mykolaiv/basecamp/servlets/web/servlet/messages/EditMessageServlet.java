package com.mykolaiv.basecamp.servlets.web.servlet.messages;

import com.mykolaiv.basecamp.servlets.model.Message;
import com.mykolaiv.basecamp.servlets.service.MessageService;
import com.mykolaiv.basecamp.servlets.utils.ValidationUtils;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebServlet("/edit")
public class EditMessageServlet extends HttpServlet {

    private final MessageService messageService = MessageService.getInstance();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idParam = request.getParameter("id");
        if (!ValidationUtils.isNumber(idParam)) {
            response.sendRedirect("all");
        } else {
            Message toBeEdited = messageService.getById(Long.parseLong(idParam));
            if (toBeEdited != null) {
                String tags = String.join(", ", toBeEdited.getTags());
                request.setAttribute("tags", tags);
            }
            request.setAttribute("message", toBeEdited);
            request.getRequestDispatcher("WEB-INF/messages/edit_message.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String idParam = request.getParameter("id");
        String text = request.getParameter("text");
        String author = request.getParameter("author");
        String tags = request.getParameter("tags");
        boolean isEdited = false;
        if (ValidationUtils.isNumber(idParam)) {
            if (text != null && !text.isEmpty() && author != null && !author.isEmpty()) {
                long id = Long.parseLong(idParam);
                if (tags != null && (tags.isEmpty() || ValidationUtils.isSuitableForTags(tags))) {
                    try {
                        isEdited = messageService.editMessage(id, text, tags, author);
                    } catch (IllegalArgumentException e) {
                        log.info("Record with id {} not found!", id);
                    }
                }
            }
        }
        if (isEdited) {
            log.info("Message was edited");
        }
        request.setAttribute("isEdited", isEdited);
        request.getRequestDispatcher("WEB-INF/messages/edit_response.jsp").forward(request, response);
    }
}
