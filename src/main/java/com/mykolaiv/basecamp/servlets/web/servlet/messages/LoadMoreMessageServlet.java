package com.mykolaiv.basecamp.servlets.web.servlet.messages;

import com.mykolaiv.basecamp.servlets.model.Message;
import com.mykolaiv.basecamp.servlets.service.MessageService;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Slf4j
@WebServlet("/loadMore")
public class LoadMoreMessageServlet extends HttpServlet {

    private final MessageService messageService = MessageService.getInstance();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("all");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String searchField = request.getParameter("searchField");
        String searchValue = request.getParameter("searchValue");
        String searchIndexParameter = request.getParameter("searchIndex");
        int startIndex = 0;
        if (searchIndexParameter != null) {
            startIndex = Integer.parseInt(searchIndexParameter);
        }
        List<Message> messages = messageService.get(searchField, searchValue, startIndex);
        request.setAttribute("messages", messages);
        request.setAttribute("searchField", searchField);
        request.setAttribute("searchValue", searchValue);
        request.getRequestDispatcher("WEB-INF/messages/loadmore_response.jsp").forward(request, response);
    }
}
