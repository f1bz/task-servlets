package com.mykolaiv.basecamp.servlets.web.servlet.messages;

import com.mykolaiv.basecamp.servlets.model.Message;
import com.mykolaiv.basecamp.servlets.service.MessageService;
import com.mykolaiv.basecamp.servlets.utils.ValidationUtils;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebServlet("/details")
public class DetailsMessageServlet extends HttpServlet {

    private final MessageService messageService = MessageService.getInstance();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idParam = request.getParameter("id");
        if (!ValidationUtils.isNumber(idParam)) {
            response.sendRedirect("all");
        } else {
            Message toBeViewed = messageService.getById(Long.parseLong(idParam));
            request.setAttribute("message", toBeViewed);
            request.getRequestDispatcher("WEB-INF/messages/details_message.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        String action = request.getParameter("action");
        if (action != null && action.equals("delete")) {
            try {
                long idToDelete = Long.parseLong(id);
                boolean isDeleted = messageService.deleteMessage(idToDelete);
                if (isDeleted) {
                    log.info("Message was deleted");
                }
                request.setAttribute("isDeleted", isDeleted);
                request.getRequestDispatcher("WEB-INF/messages/delete_response.jsp").forward(request, response);
            } catch (NumberFormatException | ServletException e) {
                log.error("Id for deletion should be a number, but was: {}", id);
                response.sendError(401, String.format("Id for deletion should be a number, but was %s",id));
            }
        } else {
            response.sendRedirect("details?id=" + id);
        }
    }
}
