package com.mykolaiv.basecamp.servlets.web.servlet.messages;

import com.mykolaiv.basecamp.servlets.service.MessageService;
import com.mykolaiv.basecamp.servlets.utils.ValidationUtils;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebServlet("/add")
public class AddMessageServlet extends HttpServlet {

    private final MessageService messageService = MessageService.getInstance();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/messages/add_message.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String text = request.getParameter("text");
        String author = request.getParameter("author");
        String tags = request.getParameter("tags");
        boolean isSent = false;
        if (text != null && !text.isEmpty() && author != null && !author.isEmpty()) {
            if (tags != null && (tags.isEmpty() || ValidationUtils.isSuitableForTags(tags))) {
                isSent = messageService.addMessage(text, tags, author);
                if (isSent) {
                    log.info("New message was sent");
                }
            }
        }
        request.setAttribute("isSent", isSent);
        request.getRequestDispatcher("WEB-INF/messages/add_response.jsp").forward(request, response);
    }
}
