package com.mykolaiv.basecamp.servlets.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class TimeFormatterUtils {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    private TimeFormatterUtils() {
    }

    public static String format(LocalDateTime localDateTime) {
        return DATE_FORMATTER.format(localDateTime);
    }
}
