package com.mykolaiv.basecamp.servlets.utils;

import java.util.regex.Pattern;

public final class ValidationUtils {

    private static final Pattern NUMBER_PATTERN = Pattern.compile("\\d+");
    private static final Pattern TAGS_PATTERN = Pattern.compile("^\\s*((\\b[a-zA-Z][a-zA-Z0-9_]*\\b)(\\s|,)*)+$");
    private static final Pattern AUTHOR_PATTERN = Pattern.compile("^(\\b[a-zA-Z][a-zA-Z0-9_]*\\b)$");

    private ValidationUtils() {
    }

    public static boolean isNumber(String source) {
        return NUMBER_PATTERN.matcher(source).matches();
    }

    public static boolean isSuitableForTags(String source) {
        return TAGS_PATTERN.matcher(source).matches();
    }

    public static boolean isSuitableForAuthor(String source) {
        return AUTHOR_PATTERN.matcher(source).matches();
    }
}
