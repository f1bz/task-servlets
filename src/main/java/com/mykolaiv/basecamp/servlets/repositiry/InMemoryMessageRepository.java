package com.mykolaiv.basecamp.servlets.repositiry;

import com.github.javafaker.Faker;
import com.mykolaiv.basecamp.servlets.model.Message;
import com.mykolaiv.basecamp.servlets.utils.TimeFormatterUtils;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

public class InMemoryMessageRepository implements MessageRepository {

    private static final int MAX_DUMMY_MESSAGES_SIZE = 20;
    public static final int MAX_MESSAGES_PER_QUERY = 10;

    private final Map<Long, Message> messagesStore = new TreeMap<>();

    public InMemoryMessageRepository() {
        initDummyMessages();
    }

    private void initDummyMessages() {
        Faker faker = new Faker();
        for (long id = 1; id <= MAX_DUMMY_MESSAGES_SIZE; id++) {
            String tag = faker.lorem().word();
            String author = faker.lorem().word();
            String text = faker.lorem().sentence();
            Message message = Message.builder()
                    .id(id)
                    .text(text)
                    .author(author)
                    .tags(Collections.singleton(tag))
                    .creationTime(LocalDateTime.now())
                    .build();
            messagesStore.put(id, message);
        }
    }

    @Override
    public Message getById(long id) {
        return messagesStore.get(id);
    }

    @Override
    public Message save(Message message) {
        return messagesStore.put(message.getId(), message);
    }

    @Override
    public void delete(long id) {
        messagesStore.remove(id);
    }

    @Override
    public List<Message> search(String searchSelector, String searchingValue, int offset) {
        Predicate<Message> searchFilter = message -> true;
        if (searchSelector != null) {
            if (searchSelector.equals("text")) {
                searchFilter = message -> containsIgnoreCase(message.getText(), searchingValue);
            }else if (searchSelector.equals("tags")) {
                searchFilter = message -> containsIgnoreCase(String.join(",", message.getTags()), searchingValue);
            }else if (searchSelector.equals("author")) {
                searchFilter = message -> containsIgnoreCase(message.getAuthor(), searchingValue);
            }else if (searchSelector.equals("date")) {
                searchFilter = message -> TimeFormatterUtils.format(message.getCreationTime()).contains(searchingValue);
            }
        }
        List<Message> filteredMessages = messagesStore.values().stream()
                .filter(searchFilter)
                .collect(Collectors.toList());
        int lastMessageIndex = (1 + offset) * MAX_MESSAGES_PER_QUERY;
        if (filteredMessages.size() < lastMessageIndex) {
            lastMessageIndex = filteredMessages.size();
        }
        return filteredMessages.subList(offset * MAX_MESSAGES_PER_QUERY, lastMessageIndex);
    }
}
