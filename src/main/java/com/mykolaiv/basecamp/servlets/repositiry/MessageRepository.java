package com.mykolaiv.basecamp.servlets.repositiry;

import com.mykolaiv.basecamp.servlets.model.Message;

import java.util.List;

public interface MessageRepository {

    List<Message> search(String searchField, String searchingValue, int offset);

    Message getById(long id);

    Message save(Message message);

    void delete(long id);

}
