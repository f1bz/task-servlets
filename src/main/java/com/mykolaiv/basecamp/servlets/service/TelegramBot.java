package com.mykolaiv.basecamp.servlets.service;

import com.mykolaiv.basecamp.servlets.model.ResponseMessage;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import static org.apache.commons.lang3.StringUtils.EMPTY;

class TelegramBot {

    private static final String BOT_API_TEMPLATE = "https://api.telegram.org/bot%s/";
    private static final String SEND_MESSAGE_TEMPLATE = BOT_API_TEMPLATE + "sendMessage?chat_id=%s&text=%s";
    private static final String EDIT_MESSAGE_TEMPLATE = BOT_API_TEMPLATE + "editMessageText?chat_id=%s&text=%s&message_id=%s";
    private static final String DELETE_MESSAGE_TEMPLATE = BOT_API_TEMPLATE + "deleteMessage?chat_id=%s&message_id=%s";

    private static final String BOT_TOKEN = "682739899:AAFWcn28G089qAVzdM6z3B98dhDLOb9KzdY";
    private static final String DESTINATION_CHANEL = "@msg_now";

    private final OkHttpClient client = new OkHttpClient();

    ResponseMessage sendMessage(String message) {
        message = getEncodedUrl(message);
        String url = String.format(SEND_MESSAGE_TEMPLATE, BOT_TOKEN, DESTINATION_CHANEL, message);
        Request request = new Request.Builder()
                .url(url)
                .build();
        return getResponseMessage(request);
    }

    private String getEncodedUrl(String message) {
        try {
            return URLEncoder.encode(message, Charset.defaultCharset().name());
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    ResponseMessage editMessage(String message, long messageId) {
        message = getEncodedUrl(message);
        String url = String.format(EDIT_MESSAGE_TEMPLATE, BOT_TOKEN, DESTINATION_CHANEL, message, messageId);
        Request request = new Request.Builder()
                .url(url)
                .build();
        return getResponseMessage(request);
    }

    ResponseMessage deleteMessage(long messageId) {
        String url = String.format(DELETE_MESSAGE_TEMPLATE, BOT_TOKEN, DESTINATION_CHANEL, messageId);
        Request request = new Request.Builder()
                .url(url)
                .build();
        return getResponseMessage(request);
    }

    private ResponseMessage getResponseMessage(Request request) {
        try (Response response = client.newCall(request).execute()) {
            return createResponseMessage(response);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private ResponseMessage createResponseMessage(Response response) throws IOException {
        ResponseBody responseBody = response.body();
        String responseJson = responseBody == null ? EMPTY : responseBody.string();
        JSONObject jsonObject = new JSONObject(responseJson);
        Long messageId = null;
        Long date = null;
        boolean isOk = jsonObject.getBoolean("ok");
        if (!jsonObject.isNull("result")) {
            Object resultObject = jsonObject.get("result");
            if (resultObject instanceof JSONObject) {
                JSONObject resultJsonObject = (JSONObject) resultObject;
                date = resultJsonObject.getLong("date");
                messageId = resultJsonObject.getLong("message_id");
            }
        }
        return new ResponseMessage(isOk, messageId, date, responseJson);
    }
}