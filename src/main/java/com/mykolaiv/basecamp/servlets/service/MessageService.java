package com.mykolaiv.basecamp.servlets.service;

import com.mykolaiv.basecamp.servlets.model.Message;
import com.mykolaiv.basecamp.servlets.model.ResponseMessage;
import com.mykolaiv.basecamp.servlets.repositiry.InMemoryMessageRepository;
import com.mykolaiv.basecamp.servlets.repositiry.MessageRepository;
import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.SPACE;

public class MessageService {

    private static final String MESSAGE_FORMAT = "AUTHOR: %s\nTEXT: %s\n\nTAGS: %s";
    private static final String MESSAGE_NO_TAGS_FORMAT = "AUTHOR: %s\nTEXT: %s";
    private static final Pattern TAGS_SPLIT_PATTERN = Pattern.compile("\\s*,+\\s*|\\s+");

    private MessageRepository messageRepository = new InMemoryMessageRepository();
    private TelegramBot telegramBot = new TelegramBot();

    private MessageService() {
    }

    public List<Message> get(String searchSelector, String value, int offset) {
        return messageRepository.search(searchSelector, value, offset);
    }

    public boolean addMessage(String sourceText, String sourceTags, String author) {
        Message message = create(sourceText, sourceTags);
        String text = message.getText();
        String textToSend = String.format(MESSAGE_NO_TAGS_FORMAT, author, text);
        if (!message.getTags().isEmpty()) {
            String tags = formatTags(message);
            textToSend = String.format(MESSAGE_FORMAT, author, text, tags);
        }
        ResponseMessage sendMessageResponse = telegramBot.sendMessage(textToSend);
        message.setId(sendMessageResponse.getId());
        message.setAuthor(author);
        message.setCreationTime(LocalDateTime.now());
        return messageRepository.save(message) == null;
    }

    private String formatTags(Message message) {
        Set<String> tagSet = message.getTags().stream()
                .map(tag -> "#" + tag)
                .collect(Collectors.toSet());
        return String.join(SPACE, tagSet);
    }

    private static Message create(String text, String tags) {
        Set<String> tagsSet = createTags(tags);
        return Message.builder()
                .text(text)
                .tags(tagsSet)
                .build();
    }

    private static Set<String> createTags(String tags) {
        Set<String> tagsSet = Collections.emptySet();
        if (StringUtils.isNotBlank(tags)) {
            List<String> tagsList = Splitter.on(TAGS_SPLIT_PATTERN)
                    .trimResults()
                    .splitToList(tags);
            tagsSet = new HashSet<>(tagsList);
        }
        return tagsSet;
    }

    public boolean editMessage(Long id, String newText, String newTags, String newAuthor) {
        try {
            Message message = messageRepository.getById(id);
            String editedMessage = String.format(MESSAGE_NO_TAGS_FORMAT, newAuthor, newText);
            message.setText(newText);
            message.setAuthor(newAuthor);
            Set<String> tagSet = createTags(newTags);
            message.setTags(tagSet);
            if (!tagSet.isEmpty()) {
                String tags = formatTags(message);
                editedMessage = String.format(MESSAGE_FORMAT, newAuthor, newText, tags);
            }
            telegramBot.editMessage(editedMessage, id);
            return messageRepository.save(message) != null;
        } catch (NullPointerException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public boolean deleteMessage(long id) {
        messageRepository.delete(id);
        telegramBot.deleteMessage(id);
        return true;
    }

    public Message getById(Long id) {
        return messageRepository.getById(id);
    }

    private static class MessageServiceLoader {
        static final MessageService INSTANCE = new MessageService();
    }

    public static MessageService getInstance() {
        return MessageServiceLoader.INSTANCE;
    }
}