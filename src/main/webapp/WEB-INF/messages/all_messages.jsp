<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://example.com/functions" prefix="f" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Show messages</title>
    <%@include file="../sources.jsp" %>
    <script>
        let pageIndex = 0;
        let lastPageIndexWithContent = 0;

        $('document').ready(function () {
            let select = "<c:out value="${searchField}"/>";
            $('#previous-page').attr('style', 'display:none;');
            if (select != "") {
                $('select').val(select);
            }
            if ($('table tbody tr').length < <c:out value="${messagesPerPage}"/>) {
                $('#previous-page').attr('style', 'display:none;');
                $('#next-page').attr('style', 'display:none;');
            }
            $('#next-page').click(function () {
                pageIndex = pageIndex + 1;
                $('#previous-page').attr('style', 'display:inline-block;');
                load(pageIndex);
            });
            $('#previous-page').click(function () {
                pageIndex = pageIndex - 1;
                if (pageIndex <= 0) {
                    pageIndex = 0;
                    $('#previous-page').attr('style', 'display:none;');
                    $('#next-page').attr('style', 'display:inline-block;');
                }
                load(pageIndex);
            });
        });

        function load(pageIndex) {
            $.ajax({
                url: 'loadMore',
                type: 'POST',
                data: {
                    searchField: $('[name=searchField]').val(),
                    searchValue: $('[name=searchValue]').val(),
                    searchIndex: pageIndex
                },
                success: function (result) {
                    $('table tbody').html(result);
                    if ($('table tbody tr').length < <c:out value="${messagesPerPage}"/>) {
                        $('#previous-page').attr('style', 'display:inline-block;');
                        $('#next-page').attr('style', 'display:none;');
                        pageIndex = lastPageIndexWithContent + 1;
                    } else {
                        lastPageIndexWithContent = pageIndex;
                        $('#next-page').attr('style', 'display:inline-block;');
                    }
                }
            });
        }
    </script>
</head>
<body>
<%@include file="../header.jsp" %>
</br>
<div class="content">
    <h1>Search</h1><br/>
    <div class="container">
        <form action="all" method="post">
            <input type="text" class="css-input" name="searchValue" placeholder="Type text to find.."
                   value="<c:out value="${searchValue}"/>">
            <select name="searchField" class="css-input" id="plan">
                <option value="none" selected disabled hidden>
                    Search by
                </option>
                <option value="text">Text</option>
                <option value="tags">Tags</option>
                <option value="date">Date</option>
                <option value="author">Author</option>
            </select>
            <input type="submit" class="myButton" value="Search"></a>
        </form>
    </div>
    <br>
    <hr>
    <br>
    <div style="float:left; margin-bottom: 6px;"><label>Messages per page: <strong class="total"><c:out
            value="${messagesPerPage}"/></strong></label><br/>
    </div>
    <div style="float:right; margin-bottom: 6px;">
        <button id="previous-page" class="pagination"><</button>
        <button id="next-page" class="pagination">></button>
    </div>
    <table>
        <thead>
        <thead>
        <tr class="table100-head">
            <th class="text-center column1">Id</th>
            <th class="column2">Text</th>
            <th class="column3">Tags</th>
            <th class="column4">Creation time</th>
            <th class="column5">Author</th>
            <th class="column6">More</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${messages}" var="message">
            <tr>
                <td class="text-center column1"><c:out value="${message.id}"/></td>
                <td class="column2">
                    <c:out value="${message.text}"/>
                </td>
                <td class="column3">
                    <c:out value="${message.tags}"/>
                </td>
                <td class="column4">
                        ${f:formatLocalDateTime(message.creationTime)}</td>
                <td class="column5">
                    <c:out value="${message.author}"/>
                </td>
                <td class="column6">
                    <a href="details?id=<c:out value="${message.id}"/>">Details</a><br/>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <br/>
</div>
</body>
</html>