﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
    <c:when test="${isDeleted}">
        <h4 class="success">The message was successfully deleted!</h4>
    </c:when>
    <c:otherwise>
        <h4 class="fail">The message wasn't deleted!</h4>
    </c:otherwise>
</c:choose>