<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Send new message</title>
    <%@include file="../sources.jsp" %>
    <script>
        $('document').ready(function () {
            $('.myButton').click(function () {
                let text = $("[name=text]").val();
                let tags = $("[name=tags]").val();
                let author = $("[name=author]").val();

                if (text != '' && author != '') {
                    if (/^[a-zA-Z][a-z0-9_]*$/.test(author)) {
                        if (/^\s*((\b[a-zA-Z][a-zA-Z0-9_]*\b)(\s|,)*)+$/.test(tags)) {
                            sendMessage(author, text, tags)
                        } else if (tags == '') {
                            sendMessage(author, text, tags)
                        } else {
                            $('#status').fadeOut();
                            $('#status').html('<h2 class="fail">Wrong tags, try again</h2>');
                            $('#status').fadeIn();
                        }
                    } else {
                        $('#status').fadeOut();
                        $('#status').html('<h2 class="fail">Wrong author, try again</h2>');
                        $('#status').fadeIn();
                    }
                } else {
                    $('#status').fadeOut();
                    $('#status').html('<h2 class="fail">Fill text and author fields</h2>');
                    $('#status').fadeIn();
                }
            });
        });

        function sendMessage(author, text, tags) {
            $.ajax({
                url: 'add',
                type: 'POST',
                data: {
                    text: text,
                    author: author,
                    tags: tags
                },
                success: function (result) {
                    $('#status').fadeOut();
                    $('#status').html(result);
                    $('#status').fadeIn();
                }
            });
        }
    </script>
</head>
<body>
<%@include file="../header.jsp" %>
</br>
<div class="content">
    <h1>Send new message</h1><br>
    <textarea name="text" class="css-input" placeholder="Text..."></textarea><br/>
    <input type="text" name="author" class="css-input" value="" placeholder="Author.."><br/>
    <input type="text" name="tags" class="css-input" value="" placeholder="Tags, separated by comma"><br/>
    <a class="myButton">Send new message</a>
    <div id="status"></div>
</div>
</body>
</html>


