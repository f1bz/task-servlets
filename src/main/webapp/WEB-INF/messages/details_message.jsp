<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://example.com/functions" prefix="f" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Details</title>
    <%@include file="../sources.jsp" %>
    <script>
        $('document').ready(function () {
            $('.deleteBtn').click(function () {
                let id = $(this).attr('message-id');
                $.ajax({
                    url: 'details?id=' + id,
                    type: 'POST',
                    data: {
                        action: 'delete',
                        id: id
                    },
                    success: function (result) {
                        $('#status').fadeOut();
                        $('#status').html(result);
                        $('#status').fadeIn();
                    }
                });
            });
        });
    </script>
</head>
<body>
<%@include file="../header.jsp" %>
</br>
<div class="content">
    <c:choose>
        <c:when test="${message==null}">
            <h4>Sorry, message not found</h4>
        </c:when>
        <c:otherwise>
            <h1>Message #<c:out value="${message.id}"/> details</h1><br/>

            <br>
            <hr>
            <br>
            <div style="text-align: left;margin-left: 40px;">
                <h2>Text</h2>
                <p id="text_value"><c:out value="${message.text}"/></p>
                <br/>
                <br/>

                <h2>Creation time</h2>
                <p id="time_value">
                        ${f:formatLocalDateTime(message.creationTime)}
                </p>
                <br/>
                <br/>

                <h2>Author</h2>
                <p id="author_value"><c:out value="${message.author}"/></p>
                <br/>
                <br/>

                <h2>Tags</h2>
                <p id="tags_value"><c:out value="${message.tags}"/></p>
                <br/>
                <br/>

                <h2>Actions</h2>
                <a href="edit?id=<c:out value="${message.id}"/>">Edit</a>
                <br/>
                <a class="deleteBtn" message-id="<c:out value="${message.id}"/>">Delete</a>
                <br/>
                <div id="status"></div>
            </div>
        </c:otherwise>
    </c:choose>
    <br/>
    <br/>
</div>
</body>
</html>