﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:choose>
    <c:when test="${isSent}">
        <h4 class="success">The message was successfully sent!</h4>
    </c:when>
    <c:otherwise>
        <h4 class="fail">The message wasn't sent!</h4>
    </c:otherwise>
</c:choose>