<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://example.com/functions" prefix="f" %>

<c:forEach items="${messages}" var="message">
    <tr>
        <td class="text-center column1"><c:out value="${message.id}"/></td>
        <td class="column2">
            <c:out value="${message.text}"/>
        </td>
        <td class="column3">
            <c:out value="${message.tags}"/>
        </td>
        <td class="column4">
                ${f:formatLocalDateTime(message.creationTime)}</td>

        </td>
        <td class="column5">
            <c:out value="${message.author}"/>

        </td>
        <td class="column6">
            <a href="details?id=<c:out value="${message.id}"/>">Details</a><br/>
        </td>
    </tr>
</c:forEach>