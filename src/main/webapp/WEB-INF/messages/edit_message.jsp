<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Edit messages</title>
    <%@include file="../sources.jsp" %>
    <script>

        $('document').ready(function () {
            $('.myButton').click(function () {
                let id = $("[name=id]").val();
                let text = $("[name=text]").val();
                let tags = $("[name=tags]").val();
                let author = $("[name=author]").val();

                if (text != '' && author != '') {
                    if (/^[a-zA-Z][a-z0-9_]*$/.test(author)) {
                        if (/^\s*((\b[a-zA-Z][a-zA-Z0-9_]*\b)(\s|,)*)+$/.test(tags)) {
                            sendMessage(id,author,text, tags)
                        } else  if (tags == '') {
                            sendMessage(id,author,text, tags)
                        } else {
                            $('#status').fadeOut();
                            $('#status').html('<h2 class="fail">Wrong tags, try again</h2>');
                            $('#status').fadeIn();
                        }
                    } else {
                        $('#status').fadeOut();
                        $('#status').html('<h2 class="fail">Wrong author, try again</h2>');
                        $('#status').fadeIn();
                    }
                } else {
                    $('#status').fadeOut();
                    $('#status').html('<h2 class="fail">Fill text and author fields</h2>');
                    $('#status').fadeIn();
                }
            });
        });

        function sendMessage(id, author, text, tags) {
            $.ajax({
                url: 'edit',
                type: 'POST',
                data: {
                    text: text,
                    author: author,
                    id: id,
                    tags: tags
                },
                success: function (result) {
                    $('#status').fadeOut();
                    $('#status').html(result);
                    $('#status').fadeIn();
                }
            });
        }
    </script>
</head>
<body>
<%@include file="../header.jsp" %>
</br>
<div class="content">
    <c:choose>
        <c:when test="${message==null}">
            <h4>Sorry, message not found</h4>
        </c:when>
        <c:otherwise>
            <h1>Edit message #<c:out value="${message.id}"/></h1><br>
            <input type="hidden" name="id" value="<c:out value="${message.id}"/>"><br/>
            <textarea name="text" class="css-input" placeholder="Text..."><c:out value="${message.text}"/></textarea><br/>
            <input type="text" name="author" class="css-input" placeholder="Author.."
            value="<c:out value="${message.author}"/>"><br/>
            <input type="text" name="tags" class="css-input" value="<c:out value="${tags}"/>"
                   placeholder="Tags, separated by comma"><br/>
            <a class="myButton">Edit message</a>
            <br/>
            <div id="status"></div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>


