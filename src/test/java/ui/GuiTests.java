package ui;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GuiTests {

    private static ChromeDriver webDriver;

    @BeforeClass
    public static void setUp() {
        webDriver = new ChromeDriver();
    }

    @AfterClass
    public static void tearDown() {
        webDriver.close();
    }

    @Test
    public void should_have_10_rows_per_page() {
        webDriver.get("localhost:8080/servlets/all");
        int actualRowsSize = webDriver.findElementsByCssSelector("tbody tr").size();
        int actualRowsPerPage = Integer.parseInt(webDriver.findElementByCssSelector("strong").getText());
        int expectedRows = 10;

        assertThat(actualRowsSize).isEqualTo(expectedRows);
        assertThat(actualRowsPerPage).isEqualTo(expectedRows);
    }

    @Test
    public void should_fail_validation_empty_text_title() {
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("tbody tr a").click();
        webDriver.findElementByCssSelector("a[href*=edit]").click();
        webDriver.findElementByCssSelector("[name=text]").clear();
        webDriver.findElementByCssSelector("a.myButton").click();
        String actual = webDriver.findElementByCssSelector("h2.fail").getText();
        String expected = "Fill text and author fields";

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_show_new_added_message() {
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("tbody tr a").click();
        webDriver.findElementByCssSelector("a[href=add]").click();
        webDriver.findElementByCssSelector("[name=text]").sendKeys("Hello world world");
        webDriver.findElementByCssSelector("[name=author]").sendKeys("user");
        webDriver.findElementByCssSelector("[name=tags]").sendKeys("tag1 tags2");
        webDriver.findElementByCssSelector("a.myButton").click();
        webDriver.get("localhost:8080/servlets/all");

        String lastRecordId = getLastRecordId();
        webDriver.get("localhost:8080/servlets/details?id=" + lastRecordId);

        assertThat(webDriver.findElementById("text_value").getText()).isEqualTo("Hello world world");
        assertThat(webDriver.findElementById("tags_value").getText()).isEqualTo("[tag1, tags2]");
        assertThat(webDriver.findElementById("author_value").getText()).isEqualTo("user");
    }

    private String getLastRecordId() {
        String lastRecordId = "";
        while (webDriver.findElementsByCssSelector("tbody tr").size() != 0) {
            List<WebElement> rows = webDriver.findElementsByCssSelector("tbody tr");
            lastRecordId = rows.get(rows.size() - 1).findElement(By.cssSelector("td")).getText();
            if (!webDriver.findElementsByCssSelector("#next-page").get(0).isDisplayed()) {
                break;
            }
            webDriver.findElementByCssSelector("#next-page").click();
        }
        return lastRecordId;
    }

    @Test
    public void should_not_show_deleted_message() {
        webDriver.get("localhost:8080/servlets/all");
        String idForDeletion = webDriver.findElementByCssSelector("tbody tr td").getText();
        webDriver.findElementByCssSelector("tbody tr a").click();
        webDriver.findElementByCssSelector("a.deleteBtn").click();
        webDriver.get("localhost:8080/servlets/all");
        String actualId = webDriver.findElementByCssSelector("tbody tr td").getText();

        assertThat(actualId).isNotEqualTo(idForDeletion);
    }

    @Test
    public void should_show_edited_message() {
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("tbody tr a").click();
        webDriver.findElementByCssSelector("a[href*=edit]").click();
        webDriver.findElementByCssSelector("[name=text]").clear();
        webDriver.findElementByCssSelector("[name=text]").sendKeys("Hello world");
        webDriver.findElementByCssSelector("a.myButton").click();
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("tbody tr a").click();
        String actual = webDriver.findElementByCssSelector("h2 + p").getText();
        String expected = "Hello world";

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_add_new_message_and_find_it_by_text() {
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("tbody tr a").click();
        webDriver.findElementByCssSelector("a[href=add]").click();
        webDriver.findElementByCssSelector("[name=text]").sendKeys("Hello world text");
        webDriver.findElementByCssSelector("[name=author]").sendKeys("user");
        webDriver.findElementByCssSelector("[name=tags]").sendKeys("tag1 tags3");
        webDriver.findElementByCssSelector("a.myButton").click();
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("[name=searchValue]").sendKeys("Hello world");
        Select select = new Select(webDriver.findElementByCssSelector("[name=searchField]"));
        select.selectByValue("text");
        webDriver.findElementByCssSelector("[type=submit]").click();
        int rowsSize = webDriver.findElementsByCssSelector("tbody tr").size();
        String lastRecordId = getLastRecordId();
        webDriver.get("localhost:8080/servlets/details?id=" + lastRecordId);

        assertThat(rowsSize).isGreaterThan(0);
        assertThat(webDriver.findElementById("text_value").getText()).isEqualTo("Hello world text");
        assertThat(webDriver.findElementById("tags_value").getText()).isEqualTo("[tag1, tags3]");
        assertThat(webDriver.findElementById("author_value").getText()).isEqualTo("user");
    }

    @Test
    public void should_add_new_message_and_find_it_by_author() {
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("tbody tr a").click();
        webDriver.findElementByCssSelector("a[href=add]").click();
        webDriver.findElementByCssSelector("[name=text]").sendKeys("Hello world text");
        webDriver.findElementByCssSelector("[name=author]").sendKeys("admin");
        webDriver.findElementByCssSelector("[name=tags]").sendKeys("tag1 tags3");
        webDriver.findElementByCssSelector("a.myButton").click();
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("[name=searchValue]").sendKeys("admin");
        Select select = new Select(webDriver.findElementByCssSelector("[name=searchField]"));
        select.selectByValue("author");
        webDriver.findElementByCssSelector("[type=submit]").click();
        int rowsSize = webDriver.findElementsByCssSelector("tbody tr").size();
        String lastRecordId = getLastRecordId();
        webDriver.get("localhost:8080/servlets/details?id=" + lastRecordId);

        assertThat(rowsSize).isGreaterThan(0);
        assertThat(webDriver.findElementById("text_value").getText()).isEqualTo("Hello world text");
        assertThat(webDriver.findElementById("tags_value").getText()).isEqualTo("[tag1, tags3]");
        assertThat(webDriver.findElementById("author_value").getText()).isEqualTo("admin");
    }

    @Test
    public void should_add_new_message_and_find_it_by_tag() {
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("tbody tr a").click();
        webDriver.findElementByCssSelector("a[href=add]").click();
        webDriver.findElementByCssSelector("[name=text]").sendKeys("Hello world text");
        webDriver.findElementByCssSelector("[name=author]").sendKeys("user");
        webDriver.findElementByCssSelector("[name=tags]").sendKeys("hahaha");
        webDriver.findElementByCssSelector("a.myButton").click();
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("[name=searchValue]").sendKeys("hahaha");
        Select select = new Select(webDriver.findElementByCssSelector("[name=searchField]"));
        select.selectByValue("tags");
        webDriver.findElementByCssSelector("[type=submit]").click();
        int rowsSize = webDriver.findElementsByCssSelector("tbody tr").size();
        String lastRecordId = getLastRecordId();
        webDriver.get("localhost:8080/servlets/details?id=" + lastRecordId);

        assertThat(rowsSize).isGreaterThan(0);
        assertThat(webDriver.findElementById("text_value").getText()).isEqualTo("Hello world text");
        assertThat(webDriver.findElementById("tags_value").getText()).isEqualTo("[hahaha]");
        assertThat(webDriver.findElementById("author_value").getText()).isEqualTo("user");
    }

    @Test
    public void should_find_no_messages_by_text() {
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("[name=searchValue]").sendKeys("Some text that is not present in database");
        Select select = new Select(webDriver.findElementByCssSelector("[name=searchField]"));
        select.selectByValue("text");
        webDriver.findElementByCssSelector("[type=submit]").click();
        int rowsSize = webDriver.findElementsByCssSelector("tbody tr").size();
        assertThat(rowsSize).isEqualTo(0);
    }

    @Test
    public void should_find_no_messages_by_tags() {
        webDriver.get("localhost:8080/servlets/all");
        webDriver.findElementByCssSelector("[name=searchValue]").sendKeys("Some tag that is not present in database");
        Select select = new Select(webDriver.findElementByCssSelector("[name=searchField]"));
        select.selectByValue("tags");
        webDriver.findElementByCssSelector("[type=submit]").click();
        int rowsSize = webDriver.findElementsByCssSelector("tbody tr").size();
        assertThat(rowsSize).isEqualTo(0);
    }
}