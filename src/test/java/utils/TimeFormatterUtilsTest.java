package utils;

import com.mykolaiv.basecamp.servlets.utils.TimeFormatterUtils;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class TimeFormatterUtilsTest {

    @Test
    public void should_be_false_while_validate_number_as_author() {
        LocalDateTime source = LocalDateTime.of(2019, 10, 15, 10, 20, 30);
        String expected = "15.10.2019 10:20:30";
        String actual = TimeFormatterUtils.format(source);
        assertThat(actual).isEqualTo(expected);
    }
}
