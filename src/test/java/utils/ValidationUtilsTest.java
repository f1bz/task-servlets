package utils;

import com.mykolaiv.basecamp.servlets.utils.ValidationUtils;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ValidationUtilsTest {

    @Test
    public void should_be_true_while_validate_number_as_number() {
        String source = "123123";
        boolean expected = true;
        boolean actual = ValidationUtils.isNumber(source);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_be_false_while_validate_string_as_number() {
        String source = "asdasd";
        boolean expected = false;
        boolean actual = ValidationUtils.isNumber(source);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_be_false_while_validate_number_as_tags() {
        String source = "123";
        boolean expected = false;
        boolean actual = ValidationUtils.isSuitableForTags(source);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_be_true_while_validate_word_as_tags() {
        String source = "asdasd";
        boolean expected = true;
        boolean actual = ValidationUtils.isSuitableForTags(source);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_be_false_while_validate_words_and_number_as_tags() {
        String source = "asdasd, asd, 123";
        boolean expected = false;
        boolean actual = ValidationUtils.isSuitableForTags(source);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_be_true_while_validate_words_separated_by_comma_as_tags() {
        String source = "asdasd,asd, asd";
        boolean expected = true;
        boolean actual = ValidationUtils.isSuitableForTags(source);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_be_true_while_validate_word_as_author() {
        String source = "asdasd";
        boolean expected = true;
        boolean actual = ValidationUtils.isSuitableForAuthor(source);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void should_be_false_while_validate_number_as_author() {
        String source = "123";
        boolean expected = false;
        boolean actual = ValidationUtils.isSuitableForTags(source);
        assertThat(actual).isEqualTo(expected);
    }
}
